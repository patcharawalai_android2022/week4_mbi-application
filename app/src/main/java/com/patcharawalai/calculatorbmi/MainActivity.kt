package com.patcharawalai.calculatorbmi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.patcharawalai.calculatorbmi.databinding.ActivityMainBinding
import kotlin.math.roundToInt
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener() { CalculatorBMI() }
        binding.Weight.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view,
            keyCode) }
        binding.High.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view,
            keyCode) }

    }
    private fun CalculatorBMI() {
        val WeightString = binding.Weight.text.toString()
        val HighString = binding.High.text.toString()
        if(HighString.trim().length<=0||WeightString.trim().length<=0){
            Toast.makeText(this@MainActivity, "Please input data!", Toast.LENGTH_SHORT).show()
            return
        }
        else {
            val weight = WeightString.toDouble()
            var high = HighString.toDouble()
            high /= 100
            val bmi = weight / (high * high)
            val formattedBMI = (bmi * 100.0).roundToInt() / 100.0
            var proportion =
                when {
                    bmi < 18.5 -> "Under Weight [ น้ำหนัก น้อยเกินไป] \nคำแนะนำ : เกิดจากนักกีฬาที่ออกกำลังกายมากและได้รับสารอาหารไม่เพียงพอ วิธีแก้ไขต้องรับประทานอาหารที่มีคุณภาพ และมีปริมาณพลังงานเพียงพอ"
                    bmi in 18.5..23.4 -> "Normal Weight [ น้ำหนัก ปกติ ] \nคำแนะนำ : มีปริมาณไขมันอยู่ในเกณฑ์ปกติ มักจะไม่ค่อยมีโรคร้าย อุบัติการณ์ของโรคเบาหวาน โรคความดันโลหิตสูง"
                    bmi in 23.5..28.4 -> "Over Weight [ น้ำหนัก เกิน ] \nคำแนะนำ : หากมีกรรมพันธ์ุเป็นโรคเบาหวานหรือไขมันในเลือดสูงต้องพยายามลดน้ำหนักให้ดัชนีมวลกายต่ำกว่า 23.5"
                    bmi in 28.5..34.9 -> "Obese (Class |) [ โรคอ้วน ระดับ1 ] \nคำแนะนำ : หากมีเส้นรอบเอวมากกว่า 90 ซม.(ชาย) 80 ซม.(หญิง) จำเป็นต้องควบคุมอาหาร และออกกำลังกาย"
                    bmi in 35.0..39.9 -> "Obese (Class ||) [ โรคอ้วน ระดับ2 ] \nคำแนะนำ : หากมีเส้นรอบเอวมากกว่าเกณฑ์ปกติจะเสี่ยงต่อการเกิดโรคสูง ต้องควบคุมอาหารและออกกำลังกายอย่างจริงจัง"
                    else -> "Obese (Class |||) [ โรคอ้วนขึ้นสูงสุด ] \nคำแนะนำ : อัตราตายสูงหากไม่ ลดน้ำหนัก ลง"
                }
            binding.bmiResult.text = "ค่า BMI: ${formattedBMI}"
            binding.proportion.text = "อยู่ในเกณฑ์: ${proportion}"
        }
    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

}
